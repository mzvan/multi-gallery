﻿	<div class="wrap"><form action="options.php" method="post" name="options">
		<h2>Select Your Settings</h2>
		<?php wp_nonce_field('update-options'); ?>
		<table class="multi-gallery-options-table" cellpadding="10">
			<tbody>
				<tr>
					<td class="MG-table-header" colspan="4">
						<?php _e("Enter sizes data", "multi_gallery")?>
					</td>
				</tr>
				<tr>
					<td>
						<label>Size name</label>
					</td>
					<td>
						<input type="text" name="multi_gallery_size_1_name" value="<?php echo $size_1["name"]; ?>" />
					</td>
					<td>
						<input type="text" name="multi_gallery_size_2_name" value="<?php echo $size_2["name"]; ?>" />
					</td>
					<td>
						<input type="text" name="multi_gallery_size_3_name" value="<?php echo $size_3["name"]; ?>" />
					</td>
				</tr>
				<tr>
					<td>
						<label>Width</label>
					</td>
					<td>
						<input type="text" name="multi_gallery_size_1_width" value="<?php echo  $size_1["width"]; ?>" />
					</td>
					<td>
						<input type="text" name="multi_gallery_size_2_width" value="<?php echo  $size_2["width"]; ?>" />
					</td>
					<td>
						<input type="text" name="multi_gallery_size_3_width" value="<?php echo  $size_3["width"]; ?>" />
					</td>
				</tr>
				<tr>
					<td>
						<label>Height</label>
					</td>
					<td>
						<input type="text" name="multi_gallery_size_1_height" value="<?php echo  $size_1["height"]; ?>" />
					</td>
					<td>
						<input type="text" name="multi_gallery_size_2_height" value="<?php echo  $size_2["height"]; ?>" />
					</td>
					<td>
						<input type="text" name="multi_gallery_size_3_height" value="<?php echo  $size_3["height"]; ?>" />
					</td>
				</tr>
				<tr>
					<td>
						<label>Crop</label>
					</td>
					<td>
						<input type="checkbox" name="multi_gallery_size_1_crop" value="enabled" <?php echo  $size_1["crop"]; ?> />
					</td>
					<td>
						<input type="checkbox" name="multi_gallery_size_2_crop" value="enabled" <?php echo  $size_2["crop"]; ?> />
					</td>
					<td>
						<input type="checkbox" name="multi_gallery_size_3_crop" value="enabled" <?php echo  $size_3["crop"]; ?> />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<?php _e("Where to add multi gallery taxonomy. Enter coma separated string of post type names. default = \"post, page\"", "multi_gallery")?>
					</td>
				</tr>
				<tr>
					<td><?php _e("Add to:", "multi_gallery"); ?></td>
					<td colspan="3">
						<input class="MG-full-size" type="text" name="multi_gallery_adding_to" value="<?php echo (get_option('multi_gallery_adding_to') ? get_option('multi_gallery_adding_to') :"post, page"); ?>" />
					</td>
				</tr>			
			</tbody>
		</table>
		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" 
			value="	multi_gallery_size_1_name,
					multi_gallery_size_1_width, 
					multi_gallery_size_1_height, 
					multi_gallery_size_1_crop,
					multi_gallery_size_2_name,
					multi_gallery_size_2_width, 
					multi_gallery_size_2_height, 
					multi_gallery_size_2_crop,
					multi_gallery_size_3_name,
					multi_gallery_size_3_width, 
					multi_gallery_size_3_height, 
					multi_gallery_size_3_crop,
					multi_gallery_adding_to" 
		/>
		<input type="submit" name="Submit" value="Update" /></form>
		
		<div>
			<p>
			<?php _e("Before clicking this backup your database. This will delete all un-used options from all taxonomies", "multi_gallery");?>
			</p>
			<a href="javascript:clean_taxonomy_options()"><?php _e("Delete all un-used options", "multi_gallery");?></a>
		</div>
	</div>

