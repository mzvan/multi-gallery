<?php
/*
    Plugin Name: Multi galery Plugin
    Description: adding gallery to taxonomy
    Author: Matej Žvan
    Version: 0.3
*/
//function prefix multi_gallery_
class multi_gallery{
	private $where_to_use = array("post", "page");
	
	public function __construct(){
		//add taxonomy
		add_action('init', array(&$this, 'init'));
		//add image size
		add_action( 'after_setup_theme', array(&$this, 'image_sizes') );
		//add menu to admin panel
		add_action('admin_menu', array(&$this, 'multi_gallery_menu'));
		//add media scripts for wp media manger -- for admin page only
		add_action('admin_print_scripts', array( $this, 'multi_gallery_media_scripts') );
		//for page only
		add_action('wp_footer', array($this, 'multi_gallery_front_scripts'));
		//add custum field functions
		add_action( 'multi_gallery_add_form_fields', array($this, 'multi_gallery_taxonomy_new_meta_field'), 10, 2 );
		add_action( 'multi_gallery_edit_form_fields', array($this, 'multi_gallery_taxonomy_edit_meta_field'), 10, 2 );		
		add_action( 'edited_multi_gallery', array($this, 'multi_gallery_save_taxonomy_meta'), 10, 2 );  
		add_action( 'create_multi_gallery', array($this, 'multi_gallery_save_taxonomy_meta'), 10, 2 );
		add_action( 'delete_multi_gallery', array($this, 'multi_gallery_delete'), 10, 3 );
		//add ajax function 
		add_action('wp_ajax_nopriv_multi_gallery_get_json', array($this, 'multi_gallery_get_json'));
		add_action('wp_ajax_multi_gallery_get_json', 		array($this, 'multi_gallery_get_json'));
		add_action('wp_ajax_nopriv_multi_gallery_get_attachment', array($this, 'multi_gallery_get_attachment'));
		add_action('wp_ajax_multi_gallery_get_attachment', 		array($this, 'multi_gallery_get_attachment'));
		add_action('wp_ajax_nopriv_multi_gallery_clean_taxonomy_options', array($this, 'multi_gallery_clean_taxonomy_options'));
		add_action('wp_ajax_multi_gallery_clean_taxonomy_options', 		array($this, 'multi_gallery_clean_taxonomy_options'));
		
	}
	public function init(){
		$args = array(
			'public' 	=> true,
			'label'		=> 'Gallery',
		);
		$this->where_to_use = $this->get_csv_to_array( get_option('multi_gallery_adding_to') );
		register_taxonomy('multi_gallery',$this->where_to_use , $args);
	}
	public function image_sizes(){
		$sizes = array();
		for($i = 1; $i<=3;$i++){
			$sizes[$i] = array(
				"name" => 	get_option('multi_gallery_size_'.$i.'_name'),
				"width" => 	get_option('multi_gallery_size_'.$i.'_width'),
				"height" => get_option('multi_gallery_size_'.$i.'_height'),
				"crop" => 	get_option('multi_gallery_size_'.$i.'_crop')
			);
			if(	$sizes[$i]["name"] != "" AND
				$sizes[$i]["width"] == intval($sizes[$i]["width"]) AND
				$sizes[$i]["height"] == intval($sizes[$i]["height"])
				){
				$crop = ( $sizes[$i]["crop"] == "enabled" ? true : false );
				add_image_size( $sizes[$i]["name"], intval($sizes[$i]["width"]), intval($sizes[$i]["height"]), $crop );
			}
		}
	}
	public function destroy(){
		//post type
		/*	options
			remove_image_size( $name );
		*/
	}
	/*
	 *CUSTUM FIELDS
	*/
	public function multi_gallery_save_taxonomy_meta( $term_id ) {
		if ( isset( $_POST['term_meta'] ) ) {
			$t_id = $term_id;
			$term_meta = get_option( "taxonomy_$t_id" );
			$cat_keys = array_keys( $_POST['term_meta'] );
			foreach ( $cat_keys as $key ) {
				if ( isset ( $_POST['term_meta'][$key] ) ) {
					$term_meta[$key] = $_POST['term_meta'][$key];
				}
			}
			// Save the option array.
			update_option( "taxonomy_$t_id", $term_meta );
		}
	}	
	public function multi_gallery_taxonomy_new_meta_field() {
		$modal_update_href = esc_url( add_query_arg( array(
			'_wpnonce' => wp_create_nonce('multi_gallery'),
		), get_home_url() ."/wp-admin/admin.php?page=multi_gallery" ) );
		include "custum_field_new.php";
	}
	public function multi_gallery_taxonomy_edit_meta_field($term) {
		$t_id = $term->term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$modal_update_href = esc_url( add_query_arg( array(
				'_wpnonce' 	=> wp_create_nonce('multi_gallery'),
				'tax_id'	=> $t_id,
			), get_home_url() ."/wp-admin/admin.php?page=multi_gallery" ) );
		
		include "custum_field_edit.php";
	}
	public function multi_gallery_delete( $term_id, $tt_id, $deleted_term ){
		// $term_id term ID ; $tt_id term taxonomy ID , $deleted_term already deleted term object
		delete_option( "taxonomy_{$term_id}" );
	}
	/*
	 * SCRIPTS AND STYLES
	 */
	public function multi_gallery_media_scripts(){
		wp_enqueue_media();
		$js_data = array(
					"home_url" => get_home_url(),
					"ajax_url" => get_home_url(). "/wp-admin/admin-ajax.php",
					);
		wp_enqueue_script( 'multi_gallery_js', plugins_url("gallery.js", __FILE__) );
		wp_localize_script( "multi_gallery_js", "MG_plugin", $js_data );
		wp_enqueue_style( 'multi_gallery_css', plugins_url("gallery.css", __FILE__) );	
	}
	public function multi_gallery_front_scripts(){
		wp_enqueue_script( 'multi_gallery_front_js', plugins_url("gallery_front.js", __FILE__) );
		wp_enqueue_style( 'multi_gallery_front_css', plugins_url("gallery_front.css", __FILE__) );
	}
	
	/*
	 * ADMIN MENU
	 */
	public function multi_gallery_menu() {
		$capability = "update_core"; //when to show multi gallery in admin should be for admins only
		add_menu_page( "page_title", "multi gallery",$capability, "multi_gallery", array(&$this, "multi_gallery_text") );
		add_submenu_page( "multi_gallery", "page_title", "options", $capability, "multi_gallery_options_page", array(&$this, "multi_gallery_options"));
	}
	public function multi_gallery_options() {
		$size_1 = array(
			"name" => 	get_option('multi_gallery_size_1_name'),
			"width" => 	get_option('multi_gallery_size_1_width'),
			"height" => get_option('multi_gallery_size_1_height'),
			"crop" => 	(get_option('multi_gallery_size_1_crop') == "enabled" ? "checked":"")
		);
		$size_2 = array(
			"name" => 	get_option('multi_gallery_size_2_name'),
			"width" => 	get_option('multi_gallery_size_2_width'),
			"height" => get_option('multi_gallery_size_2_height'),
			"crop" => 	(get_option('multi_gallery_size_2_crop') == "enabled" ? "checked":"")
		);
		$size_3 = array(
			"name" => 	get_option('multi_gallery_size_3_name'),
			"width" => 	get_option('multi_gallery_size_3_width'),
			"height" => get_option('multi_gallery_size_3_height'),
			"crop" => 	(get_option('multi_gallery_size_3_crop') == "enabled" ? "checked":"")
		);
		include "options_page.php";
	}
	public function multi_gallery_text() {
		if (isset($_REQUEST['file']) AND isset($_REQUEST['tax_id'])) { 
			check_admin_referer("multi_gallery");
			$file = absint($_REQUEST['file']);
			$tax_id = absint($_REQUEST['tax_id']);
			echo "save {$file}  to taxonomy {$tax_id}";
			
			// $options = get_option('multi_gallery', TRUE);
			// $options = absint($_REQUEST['file']);
			// update_option('multi_gallery', $options);
		}
		$modal_update_href = esc_url( add_query_arg( array(
			'_wpnonce' => wp_create_nonce('multi_gallery'),
		), get_home_url() ."/wp-admin/admin.php?page=multi_gallery" ) );
		include "text_page.php";
	}
	/*
	 * AJAX
	 */
	public function multi_gallery_get_json(){
		$post_id 	= intval($_POST['post_id']); //ID
		$terms 		= wp_get_post_terms( $post_id, "multi_gallery");
		$response 	= array();
		foreach($terms as $key => $term){
			$id = $term->term_id;
			$term_meta = get_option( "taxonomy_$id" );
			$response[$key] = array(
									"id" 		=>  $id,
									"html" 		=> array(
													"before"=> $term_meta["multi_gallery_text_before"],
													"after"=> $term_meta["multi_gallery_text_after"],
													),
									"shortcode" => $term_meta["multi_gallery_shortcode"],
									"ids"		=> $this->get_csv_to_array( $term_meta["multi_gallery_shortcode"] )
									);
		}
		header( "Content-Type: application/json" );
		echo json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}
	public function multi_gallery_get_attachment(){
		$attachment_id 	= intval($_POST['attachment_id']); //ID
		$size 			= $_POST['size'];
		
		$response = array();
		$atts_csv = preg_match ( "/,/" , $_POST['attachment_id']);
		if($atts_csv == 1){ // if contains [,] 
			$attachment_id = $this->get_csv_to_array($_POST['attachment_id']);
			foreach($attachment_id as $id){
				$id = intval($id);
				$att = wp_get_attachment_image_src( $id, $size );
				$response[$id] = array(
									"url"		=> $att[0],
									"width"		=> $att[1],
									"height"	=> $att[2],
									"resized"	=> $att[3],
									);
			
			}
		}
		else{
			$att = wp_get_attachment_image_src( $attachment_id, $size );
			$response = array(
								"url"		=> $att[0],
								"width"		=> $att[1],
								"height"	=> $att[2],
								"resized"	=> $att[3],
								);
		
		}
		header( "Content-Type: application/json" );
		echo json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	
	}
	public function multi_gallery_clean_taxonomy_options(){
		$args = array(
		  'public'   => true,
		  '_builtin' => false //only select custom taxonomies like multi_gallery.... AND exclude categories...
		  
		); 
		//get all taxonomies
		$taxonomies=get_taxonomies( $args );
		$args = array(
			'hide_empty'	=> false,
		);
		//get all terms of all taxonomies
		$all_terms = get_terms( $taxonomies, $args );
		//get all options
		$all_options = wp_load_alloptions();
		//filter options
		$taxonomies_options = array(); //array containing all options that have taxonomy_ in name
		foreach($all_options as $key => $option){
			$match = preg_match("/taxonomy_/", $key);
			if($match == 1){
				$taxonomies_options[$key] = $option;
			}
		}
		//prepare for delete
		foreach($all_terms as $value){
			$id = $value->term_id;
			unset($taxonomies_options["taxonomy_{$id}"]);
		}
		$response = array();
		//delete un-used options
		foreach($taxonomies_options as $key => $value){
			$response[$key] = delete_option( $key );
		}
		
		//response
		header( "Content-Type: application/json" );
		echo json_encode($response, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	
	}
	/*
	 * DISPLAY
	 */
	public static function display($term, $args = array()){
		$size = (isset($args["size"]) ? $args["size"] : "full");
		$lazy = (isset($args["lazy"]) ? $args["lazy"] : true);
		
		$id = $term->term_id;
		$term_meta = get_option( "taxonomy_$id" );
		$text_before 	= $term_meta["multi_gallery_text_before"];
		$text_after 	= $term_meta["multi_gallery_text_after"];
		$shortcode		= $term_meta["multi_gallery_shortcode"];
		$ids = multi_gallery::csv_to_array($shortcode);
		$html="";
		$html .="<div class='MG-text-before'>".$text_before."</div>";
		$html .= "<div class='multi_gallery'>";
		foreach($ids as $id){
			$attachment = wp_get_attachment_image_src( $id, $size ); 
			if($lazy){
				$html .= "<img src='' data-MG-src='". $attachment[0] ."'/>";
			}
			else{
				$html .= "<img src='". $attachment[0] ."' />";
			}
		}
		$html .= "</div>";
		$html .="<div class='MG-text-after'>".$text_after."</div>";
		echo $html;
	}
	public static function csv_to_array($csv){
		$array= explode(",",$csv);
		foreach($array as $k => $v){
			$array[$k] = trim($v);
		}
		return $array;
	}
	/*
	 * PRIVATE
	 */
	private function get_csv_to_array($array){
		$x= explode(",",$array);
		foreach($x as $k => $v){
			$x[$k] = trim($v);
		}
		return $x;
	}
	/*
	 * TESTING
	 */


//class end	
}
$galery = new multi_gallery();







 












?>
