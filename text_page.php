﻿<div class="wrap">
	<p>
		<a id="choose-from-library-link" href="#"
			data-update-link="<?php echo esc_attr( $modal_update_href ); ?>"
			data-choose="<?php esc_attr_e( 'Choose your images' ); ?>"
			data-update="<?php esc_attr_e( 'add gallery' ); ?>"><?php _e( 'ADD' ); ?>
		</a> |
	</p>
</div>