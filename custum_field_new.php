﻿<?php
	//values setup
	// gallery hidden field value
	$gallery_shortcode = "";
	//text before and after
	$before_content = "";
	$before_editor_id = "contentbefore";
	$before_settings = array(
						"wpautop" => true,
						"media_buttons" => false,
						"textarea_name" => "term_meta[multi_gallery_text_before]",
						"textarea_rows" => get_option('default_post_edit_rows', 10),
						"editor_css" => "",
						"editor_class" => "",
						"teeny" => false,
						"dfw" => false,
						"quicktags" => false);
	$after_content = "";
	$after_editor_id = "contentafter";
	$after_settings = array(
						"wpautop" => true,
						"media_buttons" => false,
						"textarea_name" => "term_meta[multi_gallery_text_after]",
						"textarea_rows" => get_option('default_post_edit_rows', 10),
						"editor_css" => "",
						"editor_class" => "",
						"teeny" => false,
						"dfw" => false,
						"quicktags" => false);					
						
?>
<div class="wrap">
	<div class="form-field">
		<p class="MG-description"><?php _e( 'Enter text to display before gallery', 'multi_gallery' ); ?></p>
		<?php wp_editor( $before_content, $before_editor_id, $before_settings );	?>
		<a id="choose-from-library-link" class="button MG-button" href="#"
			data-update-link="<?php echo esc_attr( $modal_update_href ); ?>"
			data-choose="<?php esc_attr_e( 'Choose your images' ); ?>"
			data-update="<?php esc_attr_e( 'add gallery' ); ?>"><?php _e( 'Gallery' ); ?>
		</a>
		<input type="hidden" 	name="term_meta[multi_gallery_shortcode]" 		id="term_meta[multi_gallery_shortcode]" value="<?php echo $gallery_shortcode;?>">
		<p class="MG-description"><?php _e( 'Enter text to display after gallery', 'multi_gallery' ); ?></p>		
		<?php wp_editor( $after_content, $after_editor_id, $after_settings );	?>
	</div>
	<script>
		//because of JS (ajax) function saving data [ tinyMCE.triggerSave(); ] has to be called before save to copy text to text area
		jQuery( ".wp-editor-container" ).mouseout(function() {
			tinyMCE.triggerSave();
		});
	</script>
</div>
