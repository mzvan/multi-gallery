﻿// based on custom-header.js
var el;
(function($) {
	var frame;
	
	$( function() {
		// Fetch available headers and apply jQuery.masonry
		// once the images have loaded.
		// Build the choose from library frame.
		$('#choose-from-library-link').click( function( event ) {
			var $el = $(this);
			event.preventDefault();
			// If the media frame already exists, reopen it.
			if ( frame ) {
				frame.open();
				console.log(this);
				console.log(frame);
				return;
			}
			
			// frame = wp.media.frames.customHeader = wp.media({
			frame = wp.media({
				id:         'multi-gallery',                
				// frame:      'post',
				// state:      'gallery-edit',
				title:      wp.media.view.l10n.editGalleryTitle,
				editing:    true,
				multiple:   true,
				library: {
					type: 'image'
				},

				// Customize the submit button.
				button: {
					text: $el.data('update'),
					close: false
				}
			});
			el = frame;
			frame.on( 'insert', function() {
				alert("insert");
				console.log(frame.state().get('selection'));
			});
			frame.on( 'update', function() { 
				alert("update");
				console.log(frame.state().get('selection'));
			});
			// frame.on( 'add remove reset', function() { 
				// alert("add remove reset");
				// console.log(frame.state().get('selection'));
			// });
			frame.on( 'content:render:edit-image', function() { 
				alert("content:render:edit-image");
				console.log(frame.state().get('selection'));
			});
			frame.on( 'toolbar:create:featured-image', function() { 
				alert("toolbar:create:featured-image");
				console.log(frame.state().get('selection'));
			});
			frame.on( 'select', function() {
				
				console.log(frame.state().get('selection'));
				// Grab the selected attachment.
				var attachment = frame.state().get('selection').first(),
					link = $el.data('updateLink');
				
				// Tell the browser to navigate to the crop step.
				// window.location = link + '&file=' + attachment.id;
			});
			frame.open();
		});
	});
}(jQuery));


multi_gallery_frame.on( 'update', function() { 
				alert("update");
				var att =  wp.media.frames.multi_gallery_frame._selection.attachments;
				var ids = "";
				for(var i = 0; i < att.models.length;i++){
					var id = att.models[i].id;
					ids += id+",";
				}
				console.log(ids);
				document.getElementById("term_meta[multi_gallery_shortcode]").value = ids;
			});
