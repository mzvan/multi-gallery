﻿var MG_objects_count = 0;
var xmg = {};
;(function($) {
	
	var defaults = {
		
		
		mode:"normal",
		lazy: false,
		//PLAY
		slideMargin:10,
		
		slideOffsetLeft: -100, // crop prev slide mg.settings.slideOffsetLeft
		//slides style
		
		//not really an option jet
		startElement:	1,
		moveSlides:		1, //move by x slides
		sameDimensions: true,	
		
		//auto updated here for code reviews 
		windowWidth: 	0,
		windowHeight: 	0,
		selector: 		0,
		originWidth: 	0,
		originHeight: 	0,
		lazyButNot:   	false, //prepared for lazy but forced normal
		cssSet:   		true, 
	};
	
	$.fn.MG = function(options){
		if(this.length == 0) return this;
		if(this.length > 1){
			this.each(function(){$(this).MG(options)});
			return this;
		}
		MG_objects_count++;
		//console.log("object count:" + MG_objects_count);
		
		var _self = this; // create reference to jQuery object
		var mg = {
		settings: defaults,
		originalElements: [], //array
		output: {
			width:		0,
			height:		0,
			html:		"",
		},	
		canvas: {
			id: 		"MG-canvas-",
			object: 	false,
			context:	false
		},
		normal:{
			transform:{
				x:0,
				y:0,
				z:0,
			},
			container:{
						id: "MG-container-",
						class: "MG-container", 
						html:{
							open:"",
							inner:"",
							close:""
						}
					},
			viewport:{
						id: "MG-viewport-",
						class: "MG-viewport", 
						html:{
							open:"",
							inner:"",
							close:""
						}
					},
			innerContainer:{
						id: "MG-inner-container-",
						class: "MG-inner-container", 
						html:{
							open:"",
							inner:"",
							close:""
						},
						width: 0,
						originalsWidth:0,
						
					},		
			},
		elements:{
			currentPosition:	0,
			current: 			false, //array key current 
			next: 				false, //array key 
			prev: 				false, //array key 
			width: 				0,
			height: 			0,
			elements:			{},
			clonesPrepend:		"", //html
			clonesApend:		"", //html
			original:			"", //html
			loadCount:			0,  //from 0 to 3*originalElements.length
			visible:			0,  //currently visible elements
		},
		coordinate: {
			x:0,
			y:0,
			z:0
		},
		
	};
		mg.init = function(){
			//joins options and defaults
			mg.setOptions();
			//self explanatory
			mg.getChildren();
			//clone elements
			mg.makeClones();
			//width and height of container
			mg.getNewDimensions();
			//select from array
			mg.setCurrentElements();
			
			//output to document
			mg.write();
		};
		mg.setOptions = function(){
			for(i in options){
				mg.settings[i] = options[i];
			}
			mg.settings.windowWidth 	= $(window).width();
			mg.settings.windowHeight 	= $(window).height();
			mg.settings.selector	 	= _self.selector;
			mg.settings.originWidth	 	= _self.width();
			mg.settings.originHeight	= _self.height();
			mg.settings._self = _self;
		};
		mg.getChildren = function(){
			var i = 0;
			$(_self).children().each(function(){
				mg.originalElements[i] = this;
				i++;
			});
		};
		mg.getNewDimensions = function(){
			mg.output.width	 	= mg.settings.originWidth; // set to origin width
			//var i=-1;
			$(mg.originalElements).each(function(){
				//set all to max
				if(mg.settings.lazy){
					mg.output.height = false;
					mg.elements.height = false;
					mg.elements.width = false;	
				}
				else{
					if(mg.output.height < this.height){
						mg.output.height = this.height;
					}
					if(mg.elements.width < this.width){
						mg.elements.width = this.width;
					}
					if(mg.elements.height < this.height){
						mg.elements.height = this.height;
					}
				}
			});	
		};
		mg.canvas.create = function(){
			mg.canvas.id += MG_objects_count;
			mg.output.html = '<canvas id="'+ mg.canvas.id +'" width="' + mg.output.width +'" height="'+ mg.output.height +'"></canvas>';
			$(_self).html(mg.output.html);
		};
		mg.addControls = function(){
			var html = "";
			html += "<div class='MG-controls'>";
			
			html += "<div class='MG_prev'>";
			html += "</div>";
			
			html += "<div class='MG_next'>";
			html += "</div>";
			
			
			html += "</div>";
			return html;
		}
		mg.makeClones = function(){
			var clonesPrepend="", clonesApend="", original="";
			for(var i =0;i < mg.originalElements.length;i++){
				var temp =  mg.originalElements[i];
				mg.elements.elements[i] = temp;												
				mg.elements.elements[(i+mg.originalElements.length)] = temp;				
				mg.elements.elements[(i+mg.originalElements.length*2)] = temp;
				mg.normal.innerContainer.width = temp.width*3+mg.settings.slideMargin*3;
					
				if(mg.settings.lazy){
					mg.elements.clonesPrepend 	+= '<img src="'+(i===mg.elements.current ? temp.dataset.mgSrc :"")+'" data-mg-src="'+temp.dataset.mgSrc+'" alt="'+ temp.alt + '" class="MG-clone    '+temp.className+'" />';
					mg.elements.clonesApend 	+= '<img src="'+(i===mg.elements.current ? temp.dataset.mgSrc :"")+'" data-mg-src="'+temp.dataset.mgSrc+'" alt="'+ temp.alt + '" class="MG-clone    '+temp.className+'" />';
					mg.elements.original 		+= '<img src="'+(i===mg.elements.current ? temp.dataset.mgSrc :"")+'" data-mg-src="'+temp.dataset.mgSrc+'" alt="'+ temp.alt + '" class="MG-original '+temp.className+'" />';
					mg.normal.innerContainer.width = false;
				}
				else{
					var _src = (temp.src != document.URL ? temp.src : temp.dataset.mgSrc);
					mg.settings.lazyButNot = (temp.src == document.URL ? true : false); //update options for css rendering
					mg.elements.clonesPrepend 	+= '<img src="'+_src+'" alt="'+ temp.alt + '" class="MG-clone    '+temp.className+'" />';
					mg.elements.clonesApend 	+= '<img src="'+_src+'" alt="'+ temp.alt + '" class="MG-clone  	 '+temp.className+'" />';
					mg.elements.original 		+= '<img src="'+_src+'" alt="'+ temp.alt + '" class="MG-original '+temp.className+'" />';

				}				
			}
			mg.elements.elements
		};
		mg.lazyLoad =function(){
			//console.log("lazy load object: " + MG_objects_count);
			mg.lazyCSS();
			mg.loadVisible();
		};
		mg.lazyCSS = function(){
			if(mg.elements.loadCount < 1){
				setTimeout(function(){ mg.lazyCSS(); }, 200);
				console.log("lazyCSS LOOP innerContainer.id:   "+_self[0].lastChild.id);
			}
			else{
				// if(mg.settings.cssSet){
				//	console.log("lazyCSS STOP innerContainer.id:   "+_self[0].lastChild.id);
					mg.normal.innerContainer.width = (mg.elements.width+mg.settings.slideMargin)* Object.keys(mg.elements.elements).length;
					mg.setCSS();
					mg.absolutePositions();
					mg.elements.visible = [];
					

				// }
				// else{
					// console.log("lazyCSS LOOP NON STOP innerContainer.id:   "+_self[0].lastChild.id);
					// setTimeout(function(){ mg.lazyCSS(); }, 200);
				
				// }
			}
			
		};
		mg.loadVisible = function(){
			if(!mg.settings.cssSet){ 
				var key = mg.elements.current-1;
				var elementWidth = mg.elements.width + mg.settings.slideMargin;
				var i=mg.normal.transform.x+mg.elements.currentPosition - elementWidth;
				var field = (mg.normal.transform.x+mg.elements.currentPosition) + mg.output.width + elementWidth;
				//console.log("visible from: "+i+" to "+field+" object: " + MG_objects_count);
				for(; i < field; i+= elementWidth){
					mg.loadImg(key);
					key++;
				}
			}
			else{
				setTimeout(function(){ mg.loadVisible(); }, 200);
			}
		};
		mg.loadImg = function(key, original){
			if (typeof original === "undefined") {
				var original = true;
			}
			var cloneKeyPrepend = key - mg.originalElements.length;
			var cloneKeyApend 	= key + mg.originalElements.length;
			//original
			var image = $("."+mg.normal.innerContainer.class,_self).children("img:nth-child("+(key)+")");
			if( (typeof image[0] !== "undefined") && (image[0].dataset.mgSrc != "done") ){
				image.attr('src', image[0].dataset.mgSrc );
				image[0].dataset.mgSrc = "done";
				image.load(function(){ 
					if(mg.elements.loadCount < 1){ 
						mg.output.height = this.height;
						mg.elements.height = this.height;
						mg.elements.width = this.width;
					}
					if(original && (key > mg.originalElements.length) ){
						mg.loadImg(cloneKeyPrepend, false);
						mg.loadImg(cloneKeyApend, false);
					}
					mg.elements.loadCount++;
					});
			}	
		};
		mg.write = function(){
			
			if(mg.settings.mode==="normal"){
				//container
				mg.normal.container.html.open 	+= "<div id='"+ mg.normal.container.id + MG_objects_count +"' class='"+mg.normal.container.class+"'>";
				mg.normal.container.html.close 	+= "</div>";
				//viewport
				mg.normal.viewport.html.open 	+= "<div id='"+ mg.normal.viewport.id + MG_objects_count +"' class='"+mg.normal.viewport.class+"'>";
				mg.normal.viewport.html.close 	+= "</div>";
				//innercontainer
				mg.normal.innerContainer.html.open 	+= "<div id='"+ mg.normal.innerContainer.id + MG_objects_count +"' class='"+mg.normal.innerContainer.class+"'>";
				mg.normal.innerContainer.html.close 	+= "</div>";
				mg.normal.viewport.html.inner += mg.normal.innerContainer.html.open + mg.elements.clonesPrepend + mg.elements.original +mg.elements.clonesApend+ mg.normal.innerContainer.html.close;
				//combine containers
				mg.normal.container.html.inner += mg.normal.viewport.html.open + mg.normal.viewport.html.inner + mg.normal.viewport.html.close;	
				mg.normal.container.html.inner += mg.addControls(); //add controls
				mg.output.html += mg.normal.container.html.open + mg.normal.container.html.inner + mg.normal.container.html.close;
				//write to [_self]
				$(_self).html( mg.output.html );				
				//tweak css 
				if(mg.settings.lazy){
					mg.loadImg(mg.elements.current);
					mg.lazyLoad();	
				}
				else{
					//people are stupid of at least clumsy
					if(mg.settings.lazyButNot){
						//console.log("Your images are prepared for lazy loading you SHOULD use it");
						mg.output.height = 0;
						mg.elements.height = 0;
						mg.elements.width = 0;	
						mg.normal.innerContainer.width = 0;
						mg.elements.loadCount = 0;
						var all =0;
						$("."+mg.normal.innerContainer.class,_self).children("img").each(function(){
							all++;
							$(this).load(function(){
								mg.normal.innerContainer.width += this.width + mg.settings.slideMargin;
								if(mg.output.height < this.height){
									mg.output.height = this.height;
								}
								if(mg.elements.height < this.height){
									mg.elements.height = this.height;
								}
								if(mg.elements.width < this.width){
									mg.elements.width = this.width;
								}
								mg.elements.loadCount++;
							});
						});
						mg.lazyButNotCssSet(mg.elements.loadCount, all); //recursive function when all is loaded set css
						
					}
					else{
						mg.setCSS();
					}
					$("img",_self).css("margin-right",mg.settings.slideMargin+"px");
				}
				//always in normal mode
				//END NORMAL
			}
			if(mg.settings.mode==="canvas"){ // not yet 
				
				mg.canvas.create();
				
				mg.canvas.object = document.getElementById(mg.canvas.id);
				mg.canvas.context = mg.canvas.object.getContext("2d");

				if(mg.settings.slideOffsetLeft === false){
					mg.settings.slideOffsetLeft = 0 - mg.elements.width;
				}
				var img, i=mg.elements.current;
				//prev
				img = mg.originalElements[mg.elements.prev];
				mg.coordinate.x = (img.width + mg.settings.slideOffsetLeft)* -1;
				mg.canvas.context.drawImage(img,mg.coordinate.x,mg.coordinate.y);
				//console.log(img.width + " + "+mg.settings.slideOffsetLeft + " = "+ img.width + mg.settings.slideOffsetLeft);
				//current
				
				while(mg.coordinate.x < mg.output.width){
					i++;
					mg.setCurrentElements(i);
					//console.log(" mg.coordinate.x < mg.output.width " + mg.coordinate.x +"<"+ mg.output.width);
					var img = mg.originalElements[mg.elements.current];
					//console.log(img);
					mg.coordinate.x += Math.abs( img.width );
					mg.canvas.context.drawImage(img,mg.coordinate.x,mg.coordinate.y);
				}
			}
			
		};
		mg.absolutePositions = function(){
			var i = 0;
			$("."+mg.normal.innerContainer.class,_self).children("img").each(function(){
				var left = (mg.elements.width + mg.settings.slideMargin ) * i;
				$(this).css({"position":"absolute","left":left+"px"});
				i++;
			});
		};
		mg.setInnerContainerOriginalElementsWidth = function(){
			mg.normal.innerContainer.originalsWidth = ( mg.originalElements.length * (mg.settings.slideMargin + mg.elements.width) ) ;
		};
		mg.setCSS = function(){
			console.log("setting CSS innerContainer.id:"+_self[0].lastChild.id);
			mg.settings.cssSet = false;
			mg.setInnerContainerOriginalElementsWidth();
			mg.normal.transform.x = 0 - mg.normal.innerContainer.originalsWidth + mg.settings.slideOffsetLeft;
			//console.log(_self);
			$(_self).css({"width":mg.output.width, "height":mg.output.height, "position":"relative"});
			$("."+mg.normal.innerContainer.class,_self).css({"width":(mg.normal.innerContainer.width/mg.output.width)*105+"%", "left":"0px"});
			$("."+mg.normal.innerContainer.class,_self).css( "-webkit-transform", "translate3d("+mg.normal.transform.x+"px, 0, 0)");
			
		}
		mg.lazyButNotCssSet = function(x, all){
			if(x==all){
				mg.setCSS();
			}
			else{
				setTimeout(function(){ mg.lazyButNotCssSet(mg.elements.loadCount, all); }, 200);
			}
		}
		mg.move = function(direction){
			var newOffset;
			var reset = $("."+mg.normal.innerContainer.class,_self).children("img:nth-child("+(mg.elements.current+1)+")").hasClass("MG-clone");
			if(direction === "next"){
				if( reset ){
					reset=true;
					newOffset = mg.normal.transform.x + mg.normal.innerContainer.originalsWidth;
					$("."+mg.normal.innerContainer.class,_self).css( "-webkit-transform", "translate3d("+newOffset+"px, 0, 0)");
					mg.setCurrentElements(mg.originalElements.length);
					
				}
				mg.elements.prev = mg.elements.current;
				mg.elements.current = mg.elements.next;
				mg.elements.next =mg.selectElement(mg.elements.next+1);
				mg.elements.currentPosition -= (mg.elements.width + mg.settings.slideMargin );
			}
			if(direction === "prev"){
				if( reset ){
					reset=true;
					newOffset = mg.normal.transform.x - mg.normal.innerContainer.originalsWidth;
					
					mg.setCurrentElements(mg.originalElements.length*2);
				}
				//log in position
				mg.elements.next = mg.elements.current;
				mg.elements.current = mg.elements.prev;
				mg.elements.prev =mg.selectElement(mg.elements.prev-1);
				mg.elements.currentPosition += (mg.elements.width + mg.settings.slideMargin );
				
			}
			if(reset){
				$("."+mg.normal.innerContainer.class,_self).css( "-webkit-transform", "translate3d("+newOffset+"px, 0, 0)");
				mg.normal.transform.x = newOffset;
			}
			//start loading
			mg.loadVisible();
			//do animation
			$("."+mg.normal.innerContainer.class,_self).css("left",mg.elements.currentPosition+"px");
		};
		mg.setCurrentElements = function(current){
			if (typeof current === "undefined") {
				current = mg.settings.startElement + mg.originalElements.length; //shift to originals 
			}
			mg.elements.current = mg.selectElement( current );
			mg.elements.next = mg.selectElement( mg.elements.current + mg.settings.moveSlides );
			mg.elements.prev = mg.selectElement( mg.elements.current - mg.settings.moveSlides );
		};
		mg.selectElement = function(key){
				var _length = Object.keys(mg.elements.elements).length;
				if(key < 0){
					key = _length + key; // negative means from last ex: -1 selects last, -2 select second last ... KEY IS NEGATIVE VALUE +-1 = -1
				}
				if(key >= _length){
					key = key - (_length ); //loops from last to first
					if(key >= _length){
						key = 0;
					}
				}
			return key;
		};
		/*
		 * INIT
		 */
		mg.init();
		/*
		 * EVENTS
		 */
		$(".MG_prev",_self).on("click", function(){
			mg.move("prev");			
		}); 
		$(".MG_next",_self).on("click", function(){
			mg.move("next");
		}); 
		console.log(mg);
		xmg[MG_objects_count] = mg;
		return this;
	}
}(jQuery));
