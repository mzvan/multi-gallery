//built with the help of tutorial on -> http://shibashake.com/
//global MG_plugin. variables object from wordpress
var hidden_field_id = "term_meta[multi_gallery_shortcode]";
(function($) {
	var multi_gallery_frame;
	$( function() {
		// Build the choose from library multi_gallery_frame.
		$('#choose-from-library-link').click( function( event ) {
			var $el = $(this);
			event.preventDefault();
			// If the media multi_gallery_frame already exists, reopen it.
			if ( multi_gallery_frame ) {
				multi_gallery_frame.open();
				return;
			}
			//else create it
			multi_gallery_frame = wp.media.frames.multi_gallery_frame = wp.media({
				id:         'multi-gallery',                
				frame:      'post',
				state:      'gallery-library',
				title:      wp.media.view.l10n.editGalleryTitle,
				editing:    true,
				multiple:   true,
				library: {
					type: 'image'
				},
			});
			//get the hidden field containing short-code
			var input_field = document.getElementById(hidden_field_id);
			//opening new wp.media
			multi_gallery_frame.open();
			//if gallery was already created fill it with attachments
			if(input_field.value != ""){
				var multi_gallery_shortcode = input_field.value;
				var att_array = multi_gallery_shortcode.split(",");
				for(var i=0;i<att_array.length;i++){
					var val = att_array[i];
					wp.media.frames.multi_gallery_frame.options.selection.add(wp.media.attachment(val));
				}
			}
			//event on gallery submit
			multi_gallery_frame.on( 'update', function() { 
				var attributes = wp.media.frames.multi_gallery_frame.options.selection.models;
				var ids = "";
				for(var i = 0; i < attributes.length;i++){
					var id = attributes[i].id;
					ids += id+",";
				}
				ids = ids.substring(0, ids.length - 1)
				input_field.value = ids;
			});
		});
	});
}(jQuery));
//ajax function
function get_taxonomy_gallery(post_id){
	var ajaxData = {
		action: 'multi_gallery_get_json',
		post_id: post_id
	};
	console.log(ajaxData);
	jQuery.ajax({
		url: MG_plugin.ajax_url,
		type: 'POST',
		data: ajaxData,
		dataType: 'json'
	})	.done(function(data) {
			console.log( "ajax request succeeded" );
			console.log( data );
			var json = eval(data);

		})
		.fail(function() {
			alert( "error" );
		})
		.always(function() {
		});
}
function get_attachment_data(attachment_id, size){
	var ajaxData = {
		action: 'multi_gallery_get_attachment',
		attachment_id: attachment_id,
		size: size
	};
	console.log(ajaxData);
	jQuery.ajax({
		url: MG_plugin.ajax_url,
		type: 'POST',
		data: ajaxData,
		dataType: 'json'
	})	.done(function(data) {
			console.log( "ajax request succeeded" );
			console.log( data );
			var json = eval(data);

		})
		.fail(function() {
			alert( "error" );
		})
		.always(function() {
		});
}
function clean_taxonomy_options(){
	var ajaxData = {
		action: 'multi_gallery_clean_taxonomy_options',
	};
	console.log(ajaxData);
	jQuery.ajax({
		url: MG_plugin.ajax_url,
		type: 'POST',
		data: ajaxData,
		dataType: 'json'
	})	.done(function(data) {
			console.log( "ajax request succeeded" );
			console.log( data );
			var json = eval(data);

		})
		.fail(function() {
			alert( "error" );
		})
		.always(function() {
		});
}				